package com.cc3002.breakout.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.facade.HomeworkTwoFacade;
import com.cc3002.breakout.logic.brick.IBrick;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class BrickTest {

  private HomeworkTwoFacade facade;

  @Before
  public void setUp() {
    facade = new HomeworkTwoFacade();
  }

  @Test
  public void numberOfBricksTest() {
    facade.setCurrentLevel(facade.newLevelWithSoftAndStoneBricks("Test Level", 10, 0.2));
    assertEquals(10, facade.numberOfBricks());
  }

  @Test
  public void addScoreAfterSoftBrickDestroyed() {
    facade.setCurrentLevel(facade.newLevelWithSoftAndStoneBricks("One Soft Brick", 1, 1.0));
    assertEquals(0, facade.earnedScore());
    List<IBrick> bricks = facade.getBricks();
    assertFalse(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(10, facade.earnedScore());
    assertTrue(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(10, facade.earnedScore());
    assertTrue(bricks.get(0).isDestroyed());
  }

  @Test
  public void addScoreAfterStoneBrickDestroyed() {
    facade.setCurrentLevel(facade.newLevelWithSoftAndStoneBricks("One Stone Brick", 1, 0));
    assertEquals(0, facade.earnedScore());
    List<IBrick> bricks = facade.getBricks();
    assertFalse(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(0, facade.earnedScore());
    assertFalse(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(0, facade.earnedScore());
    assertFalse(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(50, facade.earnedScore());
    assertTrue(bricks.get(0).isDestroyed());
    bricks.get(0).hit();
    assertEquals(50, facade.earnedScore());
    assertTrue(bricks.get(0).isDestroyed());
  }

}
