package com.cc3002.breakout.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.facade.HomeworkTwoFacade;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.level.ILevel;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;


public class LevelTest {

  private HomeworkTwoFacade facade;

  @Before
  public void setUp() {
    facade = new HomeworkTwoFacade();
    facade.setCurrentLevel(facade.newLevelWithSoftAndStoneBricks("Test Level", 10, 0.2));
  }

  @Test
  public void newLevelTest() {
    assertEquals("Test Level", facade.getLevelName());
    assertEquals(10, facade.numberOfBricks());
    assertTrue(facade.hasCurrentLevel());
  }

  @Test
  public void newLevelWithoutSoftBricksTest() {
    ILevel testLevel = facade.newLevelWithSoftAndStoneBricks("No Soft Bricks", 10, 0);
    assertEquals(0, getNumberOfSoftBricks(testLevel));
    assertEquals(10, getNumberOfStoneBricks(testLevel));
  }

  @Test
  public void newLevelWithoutStoneBricksTest() {
    ILevel testLevel = facade.newLevelWithSoftAndStoneBricks("No Stone Bricks", 10, 1);
    assertEquals(10, getNumberOfSoftBricks(testLevel));
    assertEquals(0, getNumberOfStoneBricks(testLevel));
  }

  @Test
  public void requiredPointsTest() {
    int expected = (int) (0.7 * (10 * getNumberOfSoftBricks(facade.getCurrentLevel())
        + 50 * getNumberOfStoneBricks(facade.getCurrentLevel())));
    assertEquals(expected, facade.getRequiredPoints());
  }

  @Test
  public void hasNextLevelTest() {
    assertFalse(facade.hasNextLevel());
    facade.newLevelWithSoftAndStoneBricks("Second Level", 20, 0.5);
    assertTrue(facade.hasNextLevel());
  }

  @Test
  public void nullLevelTest() {
    facade.setNextLevel();
    ILevel nullLevel = facade.getCurrentLevel();
    nullLevel.addPoints(10);
    nullLevel.registerBonuses(facade.newBonuses(10, 0.5));
    nullLevel.setNextLevel(facade.newLevelWithSoftAndStoneBricks("New Level", 10, 0.2));
    nullLevel.setRequiredPoints(50);
    assertFalse(nullLevel.isLevel());
    assertNull(nullLevel.getBricks());
    assertNull(nullLevel.getBricksIterator());
    assertNull(nullLevel.getBonuses());
    assertEquals(0, nullLevel.getCurrentPoints());
    assertEquals("", nullLevel.getLevelName());
    assertEquals(nullLevel, nullLevel.getNextLevel());
    assertEquals(0, nullLevel.getNumberOfBricks());
    assertEquals(0, nullLevel.getRequiredPoints());
    assertFalse(nullLevel.hasNextLevel());

  }

  private int getNumberOfSoftBricks(final ILevel level) {
    int count = 0;
    Iterator<IBrick> bricksIterator = level.getBricksIterator();
    while (bricksIterator.hasNext()) {
      if (bricksIterator.next().isSoftBrick()) {
        count++;
      }
    }
    return count;
  }

  private int getNumberOfStoneBricks(final ILevel level) {
    int count = 0;
    Iterator<IBrick> bricksIterator = level.getBricksIterator();
    while (bricksIterator.hasNext()) {
      if (bricksIterator.next().isStoneBrick()) {
        count++;
      }
    }
    return count;
  }

}
