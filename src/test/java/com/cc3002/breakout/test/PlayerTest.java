package com.cc3002.breakout.test;

import static org.junit.Assert.assertEquals;

import com.cc3002.breakout.facade.HomeworkTwoFacade;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

  private HomeworkTwoFacade facade;

  @Before
  public void setUp() {
    facade = new HomeworkTwoFacade();
  }

  @Test
  public void decrementHeartsTest() {
    assertEquals(3, facade.getNumberOfHearts());
    assertEquals(2, facade.lossOfHeart());
  }

}
