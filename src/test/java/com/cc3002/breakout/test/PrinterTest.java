package com.cc3002.breakout.test;

import static org.junit.Assert.assertEquals;

import com.cc3002.breakout.facade.HomeworkTwoFacade;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.brick.SoftBrick;
import com.cc3002.breakout.logic.brick.StoneBrick;
import com.cc3002.breakout.view.Printer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PrinterTest {

  private HomeworkTwoFacade facade;

  @Before
  public void setUp() {
    facade = new HomeworkTwoFacade();
  }

  @Test
  public void printSoftBrickTest() {
    assertEquals("*",
        facade.spawnBricks(facade.newLevelWithSoftAndStoneBricks("One Soft Brick", 1, 1)));
  }

  @Test
  public void printStoneBrickTest() {
    assertEquals("#",
        facade.spawnBricks(facade.newLevelWithSoftAndStoneBricks("One Stone Brick", 1, 0)));
  }

  @Test
  public void printMixedBricksTest() {
    List<IBrick> bricks = new ArrayList<IBrick>();
    Printer printer = new Printer();
    // First Row of 16 bricks
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    // Second Row of 16 bricks
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new StoneBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new SoftBrick());
    bricks.add(new StoneBrick());

    String expected =
        "***#*##***#*###*" + System.lineSeparator() + "*##*#****###***#" + System.lineSeparator();
    assertEquals(expected, printer.print(bricks));
  }

}
