package com.cc3002.breakout.view;

import com.cc3002.breakout.logic.brick.IBrick;

import java.util.Iterator;
import java.util.List;

/**
 * Printer displays a set of bricks as a string, using predefined characters for each type.
 * 
 * @author SebastianCisneros
 *
 */
public class Printer {

  private static final char SOFTBRICK_CHAR = '*';
  private static final char STONEBRICK_CHAR = '#';

  /**
   * Prints every brick with its corresponding symbol. Every 16 symbols prints a new line. Makes use
   * of double-dispatch.
   * 
   * @author SebastianCisneros
   * @param bricks List of bricks to be printed.
   * @return Returns a 16-width String representing bricks.
   *
   */
  public String print(final List<IBrick> bricks) {
    final StringBuffer stream = new StringBuffer();
    final Iterator<IBrick> bricksIterator = bricks.iterator();
    int brickCount = 0;
    while (bricksIterator.hasNext()) {
      final IBrick brick = bricksIterator.next();
      brick.print(this, stream);
      brickCount++;
      brickCount %= 16;
      if (brickCount == 0) {
        stream.append(System.lineSeparator());
      }
    }
    return stream.toString();
  }

  public void printSoftBrick(final StringBuffer stream) {
    stream.append(SOFTBRICK_CHAR);
  }

  public void printStoneBrick(final StringBuffer stream) {
    stream.append(STONEBRICK_CHAR);
  }

}
