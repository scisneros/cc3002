package com.cc3002.breakout.facade;

import com.cc3002.breakout.logic.bonus.BonusFactory;
import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.game.Game;
import com.cc3002.breakout.logic.level.ILevel;
import com.cc3002.breakout.logic.player.Player;
import com.cc3002.breakout.view.GameConsole;
import com.cc3002.breakout.view.Printer;

import java.io.PrintStream;
import java.util.List;

/**
 * Facade controls the game logic.
 * 
 * @author SebastianCisneros
 *
 */
public class HomeworkTwoFacade {

  private final Game game = new Game();
  private final Player player = new Player();
  private final Printer printer = new Printer();
  private final BonusFactory bonusFactory = new BonusFactory();
  private final GameConsole console = new GameConsole(game);

  /**
   * Generates a new level with given name, number of bricks and chances of getting Soft Bricks. New
   * level is added to levels list.
   * 
   * @author SebastianCisneros
   *
   */
  public ILevel newLevelWithSoftAndStoneBricks(final String levelName, final int number,
      final double probability) {
    return game.addLevel(levelName, number, probability);
  }

  public long numberOfBricks() {
    return getCurrentLevel().getNumberOfBricks();
  }

  public List<IBrick> getBricks() {
    return game.getBricks();
  }

  public boolean hasNextLevel() {
    return game.hasNextLevel();
  }

  public String getLevelName() {
    return getCurrentLevel().getLevelName();
  }

  public int getRequiredPoints() {
    return getCurrentLevel().getRequiredPoints();
  }

  public int getNumberOfHearts() {
    return player.getHearts();
  }

  public int lossOfHeart() {
    return player.decrementHearts();
  }

  public long earnedScore() {
    return getCurrentLevel().getCurrentPoints();
  }

  public ILevel getCurrentLevel() {
    return game.getCurrentLevel();
  }

  public void setCurrentLevel(final ILevel newLevel) {
    game.setCurrentLevel(newLevel);
  }

  public String spawnBricks(final ILevel level) {
    return printer.print(level.getBricks());
  }

  public void setNextLevel() {
    game.setNextLevel();
  }

  public List<IBonus> newBonuses(final int number, final double probability) {
    return bonusFactory.generateBonuses(number, probability, player, getCurrentLevel());
  }

  public void registerBonuses(final List<IBonus> bonuses) {
    getCurrentLevel().registerBonuses(bonuses);
  }

  public void setGameConsoleOutput(final PrintStream printStream) {
    console.setOutput(printStream);
  }

  public void autoSwitchToNextLevel() {
    game.autoSwitchToNextLevel();
  }

  public boolean hasCurrentLevel() {
    return getCurrentLevel().isLevel();
  }

  public IBonus newExtraScore() {
    return bonusFactory.newExtraScore(getCurrentLevel());
  }

  public IBonus newExtraHeart() {
    return bonusFactory.newExtraHeart(player);
  }

  public IBonus newScoreDiscount() {
    return bonusFactory.newScoreDiscount(getCurrentLevel());
  }

  public IBonus newHeartDiscount() {
    return bonusFactory.newHeartDiscount(player);
  }

}
