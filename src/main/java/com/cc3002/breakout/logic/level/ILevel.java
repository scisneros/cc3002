package com.cc3002.breakout.logic.level;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.view.Printable;

import java.util.Iterator;
import java.util.List;

/**
 * Interface for level objects.
 * 
 * @author SebastianCisneros
 *
 */
public interface ILevel extends Printable {

  ILevel addLevel(ILevel nextLevel);

  void addPoints(int points);

  List<IBrick> getBricks();

  Iterator<IBrick> getBricksIterator();

  long getCurrentPoints();

  String getLevelName();

  ILevel getNextLevel();

  int getNumberOfBricks();

  int getRequiredPoints();

  boolean hasNextLevel();

  boolean isLevel();

  void registerBonuses(List<IBonus> bonuses);

  void setNextLevel(ILevel nextLevel);

  void setRequiredPoints(int points);

  List<IBonus> getBonuses();


}
