package com.cc3002.breakout.logic.level;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.AbstractBrick;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.brick.SoftBrick;
import com.cc3002.breakout.logic.brick.StoneBrick;
import com.cc3002.breakout.view.GameConsole;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

/**
 * Level contains a list of the corresponding bricks. Has a name and score according to bricks.
 * 
 * @author SebastianCisneros
 *
 */
public class Level extends Observable implements Observer, ILevel {
  private final String name;
  private final List<IBrick> bricks = new ArrayList<IBrick>();
  private List<IBonus> bonuses = new ArrayList<IBonus>();
  private ILevel next = new NullLevel();
  private int totalPoints;
  private int requiredPoints;
  private int currentPoints = 0;
  private final Random randomGenerator = new Random();

  public Level(final String levelName, final int number, final double probability) {
    name = levelName;
    generateBricks(number, probability);
  }

  private void generateBricks(final int number, final double probability) {
    AbstractBrick newBrick;
    for (int i = 0; i < number; i++) {
      final double randomBrick = randomGenerator.nextDouble();
      if (randomBrick <= probability) {
        newBrick = new SoftBrick();
      } else {
        newBrick = new StoneBrick();
      }
      newBrick.addObserver(this);
      bricks.add(newBrick);
      totalPoints += newBrick.getScore();
    }
    setRequiredPoints((int) (totalPoints * 0.7));
  }

  public List<IBrick> getBricks() {
    return bricks;
  }

  public Iterator<IBrick> getBricksIterator() {
    return bricks.iterator();
  }

  public String getLevelName() {
    return name;
  }

  public int getNumberOfBricks() {
    return bricks.size();
  }

  public int getRequiredPoints() {
    return requiredPoints;
  }

  public long getCurrentPoints() {
    return currentPoints;
  }

  public ILevel getNextLevel() {
    return next;
  }

  public List<IBonus> getBonuses() {
    return bonuses;
  }

  public void setRequiredPoints(int points) {
    requiredPoints = points;
  }

  public void setNextLevel(ILevel nextLevel) {
    next = nextLevel;
  }

  /**
   * Add points to this level's current points. Can substract points, also.
   * 
   * @author SebastianCisneros
   *
   */
  public void addPoints(int points) {
    currentPoints += points;
    setChanged();
    notifyObservers();
  }

  public ILevel addLevel(ILevel nextLevel) {
    setNextLevel(next.addLevel(nextLevel));
    return this;
  }

  public boolean hasNextLevel() {
    return next.isLevel();
  }

  public boolean isLevel() {
    return true;
  }

  /**
   * This method will trigger whenever a brick of this level is destroyed, adding corresponding
   * score.
   * 
   * @author SebastianCisneros
   *
   */
  public void update(Observable destroyedBrick, Object brickPoints) {
    setChanged();
    notifyObservers(destroyedBrick);
    addPoints((Integer) brickPoints);
  }

  public void registerBonuses(List<IBonus> bonuses) {
    this.bonuses = bonuses;
  }

  public void output(GameConsole console) {
    console.printLevel(getLevelName());
  }

}
