package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.view.GameConsole;
import com.cc3002.breakout.view.Printer;

/**
 * SoftBrick is a type of brick with 1 hitpoint and worth 10 points.
 * 
 * @author SebastianCisneros
 *
 */
public class SoftBrick extends AbstractBrick {

  /**
   * Constructor defines default parameters for this brick type.
   * 
   * @author SebastianCisneros
   *
   */
  public SoftBrick() {
    super();
    hitPoints = 1;
    score = 10;
  }

  public boolean isSoftBrick() {
    return true;
  }

  public boolean isStoneBrick() {
    return false;
  }

  public void print(final Printer printer, final StringBuffer stream) {
    printer.printSoftBrick(stream);
  }

  public void output(GameConsole console) {
    console.printSoftBrickDestroyed(getScore());
  }

}
