package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.view.Printable;

import java.util.Observable;

/**
 * AbstractBrick defines common behavior for real Bricks.
 * 
 * @author SebastianCisneros
 *
 */
public abstract class AbstractBrick extends Observable implements IBrick, Printable {

  protected int hitPoints;
  protected int score;
  protected boolean destroyed = false;

  public int getScore() {
    return score;
  }

  /**
   * This method decrease brick's hitpoints by 1 and notifies its level if it is destroyed.
   * 
   * @author SebastianCisneros
   *
   */
  public void hit() {
    hitPoints--;
    if (remainingHits() == 0) {
      setChanged();
      notifyObservers(score);
      destroyed = true;
    }
  }

  public int remainingHits() {
    return hitPoints;
  }

  public boolean isDestroyed() {
    return destroyed;
  }

}
