package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.view.Printer;

/**
 * Interface for brick objects.
 * 
 * @author SebastianCisneros
 *
 */
public interface IBrick {

  int getScore();

  boolean isSoftBrick();

  boolean isStoneBrick();

  void hit();

  int remainingHits();

  void print(Printer printer, StringBuffer stream);

  boolean isDestroyed();

}
