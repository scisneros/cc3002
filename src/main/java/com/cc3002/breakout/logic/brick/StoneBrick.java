package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.view.GameConsole;
import com.cc3002.breakout.view.Printer;

/**
 * StoneBrick is a type of brick with 3 hitpoints and worth 50 points.
 * 
 * @author SebastianCisneros
 *
 */
public class StoneBrick extends AbstractBrick {

  /**
   * Constructor defines default parameters for this brick type.
   * 
   * @author SebastianCisneros
   *
   */
  public StoneBrick() {
    super();
    hitPoints = 3;
    score = 50;
  }

  public boolean isSoftBrick() {
    return false;
  }

  public boolean isStoneBrick() {
    return true;
  }

  public void print(final Printer printer, final StringBuffer stream) {
    printer.printStoneBrick(stream);
  }

  public void output(GameConsole console) {
    console.printStoneBrickDestroyed(getScore());
  }

}
