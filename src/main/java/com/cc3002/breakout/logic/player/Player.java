package com.cc3002.breakout.logic.player;

/**
 * Player class defines player remaining lifes.
 * 
 * @author SebastianCisneros
 *
 */
public class Player {

  private int hearts = 3;

  public int getHearts() {
    return hearts;
  }

  public int decrementHearts() {
    return --hearts;
  }

  public int incrementHearts() {
    return ++hearts;
  }

}
